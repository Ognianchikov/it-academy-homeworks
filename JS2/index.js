var countries = [
    {name: 'Belarus', capital: 'Minsk'},
    {name: 'Russia', capital: 'Moskow'},
]

var enterInfoBtn = document.querySelector('#enterInfo');
console.log(enterInfoBtn);

function askCountry() {
    return prompt('Введите название страны');
}

function askCapital() {
    return prompt('Введите название столицы');
}

enterInfoBtn.addEventListener('click', addCountry);

function addCountry() {
    var country = {};
    country.name = askCountry();
    country.capital = askCapital();
    countries.push(country);
    console.log(countries);
}

var countryInfoBtn = document.querySelector('#countryInfo');

function getCountryInfo() {
    var countryName = askCountry();
    console.log(countryName);
    var requestCountry = countries.find(function(country) {
        return country.name === countryName;
    })
    console.log(requestCountry);
    if (requestCountry) {
        console.log(requestCountry.capital);
    } else {
        console.log('Нет такой страны');
    }
}

countryInfoBtn.addEventListener('click', getCountryInfo);

var showCountriesBtn = document.querySelector('#collection');

function showCountries() {
    console.log(countries);
}

showCountriesBtn.addEventListener('click', showCountries);

var deleteInfoBtn = document.querySelector('#deleteInfo');
    console.log(deleteInfoBtn);

deleteInfoBtn.addEventListener('click', delCountryInfo);

function delCountryInfo() {
    var delCountryName = askCountry();
    var delCountry = countries.filter(function(country) {
        return delCountryName !== country.name;
        delete countries[delCountry];
    });

    if (countries.length === delCountry.length) {
        console.log('Нет такой страны.');
    } else {
        console.log('Страна ' + delCountryName + ' удалена.');
    }
}

